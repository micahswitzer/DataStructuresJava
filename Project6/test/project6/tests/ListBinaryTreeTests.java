package project6.tests;

import project6.*;

import org.junit.Test;
import org.junit.BeforeClass;
import static org.junit.Assert.*;

/**
 * Test class for the ListBinaryTree class
 * @author Micah Switzer
 */
public class ListBinaryTreeTests {
    
    private static ListBinaryTree tree;
    
    @BeforeClass
    public static void setup() {
        // set up the tree ahead of time
        //  so we don't have to do it before every test
        tree = new ListBinaryTree();
        tree.fillTree();
    }
    
    @Test
    public void validRoot() {
        assertTrue(tree.isRoot(tree.root()));
    }
    
    @Test
    public void leftChild() {
        Position root = tree.root();
        assertEquals(1, tree.leftChild(root).element());
    }
    
    @Test
    public void rightChild() {
        Position root = tree.root();
        assertEquals(2, tree.rightChild(root).element());
    }
    
    @Test
    public void sibblingChild() {
        Position leftChild = tree.leftChild(tree.root());
        assertEquals(2, tree.sibling(leftChild).element());
    }
    
    @Test
    public void parent() {
        Position firstChild = tree.leftChild(tree.root());
        assertEquals(0, tree.parent(firstChild).element());
    }
    
    @Test
    public void isExternal() {
        // get node 7 since we know it's external
        Position node7 = tree.leftChild(tree.leftChild(
                tree.leftChild(tree.root())));
        assertTrue(tree.isExternal(node7));
    }
    
    @Test
    public void isInternal() {
        // get node 4 sine we know it's internal
        Position node4 = tree.sibling(tree.leftChild(
                tree.leftChild(tree.root())));
        assertTrue(tree.isInternal(node4));
    }    
}

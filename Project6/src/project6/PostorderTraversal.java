package project6;

/**
 * Extends the Euler Tour class to implement
 *  a Post-order traversal of a tree
 * @author Micah Switzer
 */
public class PostorderTraversal extends EulerTour {
    
    public PostorderTraversal(BinaryTree newTree) {
        super(newTree);
    }
    
    /**
     * Executes the Euler tour
     */
    public void execute() {
        System.out.println("Postorder Traversal:");
        this.performTour(tree.root());
    }
    
    @Override
    protected void visitPostorder (Position pos, TraversalResult result) {
        System.out.println(pos.element());
    }
    
    @Override
    protected void visitExternal(Position pos, TraversalResult result) {
        System.out.println(pos.element());
    }
}

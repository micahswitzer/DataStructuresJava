package project6;

/**
 * Extends the Euler Tour class to implement
 *  a Pre-order traversal of a tree
 * @author Micah Switzer
 */
public class PreorderTraversal extends EulerTour {
    
    public PreorderTraversal(BinaryTree newTree) {
        super(newTree);
    }
    
    /**
     * Executes the Euler tour
     */
    public void execute() {
        System.out.println("Preorder Traversal:");
        this.performTour(tree.root());
    }
    
    @Override
    protected void visitPreorder (Position pos, TraversalResult result) {
        System.out.println(pos.element());
    }
    
    @Override
    protected void visitExternal(Position pos, TraversalResult result) {
        System.out.println(pos.element());
    }
}

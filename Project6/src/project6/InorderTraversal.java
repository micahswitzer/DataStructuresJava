package project6;

/**
 * Extends the Euler Tour class to implement
 *  an In-order traversal of a tree
 * @author Micah Switzer
 */
public class InorderTraversal extends EulerTour {
    
    public InorderTraversal(BinaryTree newTree) {
        super(newTree);
    }
    
    /**
     * Executes the Euler tour
     */
    public void execute() {
        System.out.println("Inorder Traversal:");
        this.performTour(tree.root());
    }
    
    @Override
    protected void visitInorder(Position pos, TraversalResult result) {
        System.out.println(pos.element());
    }
    
    @Override
    protected void visitExternal(Position pos, TraversalResult result) {
        System.out.println(pos.element());
    }
}

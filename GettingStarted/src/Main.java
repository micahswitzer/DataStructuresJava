public class Main
{ 
    public static void main(String[] args)
    {
        final TestClass testClass = new TestClass();

        Runnable test = () -> System.exit(2);

        test.run();

        System.out.println("Hello World!");
        testClass.TestMethod();

        System.exit(1);
    }
}

class TestClass
{

    public TestClass()
    {
        System.out.println("TestClass Constructor");
    }

    public void TestMethod()
    {
        System.out.println("TestMethod Called");
    }

}
package numeric;

/**
* This class provides a method to compute the greatest common devisor
* of two integers.
*
* @author Micah Switzer
* @version 1.0
* File: GCD.java
* Created: September 2017
*/
public class GCD
{
   /**
   * Computes the GCD of two integers
   *
   * @param x The first integer
   * @param y The second integer
   * @returns the GCD of the two numbers
   */
   public static int computeGcd(int x, int y)
   {
      if (y == 0)
      {
         return Math.abs(x);
      }
      else
      {
         return computeGcd(y, x % y);
      }
   }

   public static void main(String[] args)
   {
      // check for the proper number of arguments
      if (args.length != 2)
      {
         System.out.println("Invalid number of arguments");
         return;
      }
      int x = 0, y = 0;
      try
      {
         // try to parse the arguments
         x = Integer.parseInt(args[0]);
         y = Integer.parseInt(args[1]);
      }
      catch (java.lang.NumberFormatException ex)
      {
         // print an error message
         System.out.println("The arguments must be integers greater than zero");
         return;
      }
      // finally compute the GCD
      System.out.println(computeGcd(x, y));
   }
}

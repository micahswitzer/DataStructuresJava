package numeric;

/**
* This class contains a method to compute the factorial of an integer
*
* @author Micah Switzer
* @version 1.0
* File: Factorial.java
* Created: September 2017
*/
public class Factorial
{
   /**
   * Computes the factorial of an integer
   *
   * @param x The integer to compute the factorial of
   * @return The computed factorial
   * @throws NumberFormatException If the number is less than zero
   */
   public static double computeFactorial(int x)
   {
      if (x < 0)
      {
         // throw an exception if the supplied number was less than zero
         throw new java.lang.NumberFormatException(
                 "x must be greater than zero");
      }
      double fac = 1;
      for (int i = 2; i <= x; i++)
      {
         // iterate through all integers between 2 and x
         // and then multiply fac by that number
         fac *= i;
      }
      return fac;
   }

   public static void main(String[] args)
   {
      // check for the correct number of arguments
      if (args.length != 1)
      {
         System.out.println("Please provide exactly one (1) argument");
         return;
      }
      int x = 0;
      try
      {
          // try to parse the args and then compute the factorial
          x = Integer.parseInt(args[0]);
          System.out.println(computeFactorial(x));
      }
      catch (java.lang.NumberFormatException ex)
      {
          // print an error error message if there was an error
          System.out.println("You must enter a number greater than zero");
      }
   }
}

package project5;

/**
 * Thrown when invalid data is provided
 * @author Micah Switzer
 */
public class InvalidDataException extends Exception {
    public InvalidDataException(String message) {
        super(message);
    }
}
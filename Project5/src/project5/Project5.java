package project5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

/**
 * Project 5 - Solves a maze using Breadth First Search
 * @author Micah Switzer
 */
public class Project5 {

    /**
     * Process a map file and fins the shortest distance to the target
     * @param args the command line arguments
     * @throws project5.InvalidDataException
     *   when invalid maze data is found
     * @throws java.io.FileNotFoundException
     * when the provided file can't be found
     */
    public static void main(String[] args)
            throws InvalidDataException, FileNotFoundException {
        if (args.length < 1) {
            throw new IllegalArgumentException(
                    "A map file must be provided as an argument");
        }
        String fileName = args[0];
        Scanner scanner = new Scanner(new File(fileName));
        
        if (!scanner.hasNextInt()) {
            throw new InvalidDataException("Maze size not provided");
        }
        int rows = scanner.nextInt();
        
        if (!scanner.hasNextInt()) {
            throw new InvalidDataException("Maze size not provided");
        }
        int cols = scanner.nextInt();
        
        boolean hasStart = false;
        boolean hasFinish = false;
        
        Location[][] maze = new Location[rows][cols];
        Location startLocation = null;
        
        scanner.nextLine();
        
        for (int i = 0; i < rows; i++) {
            if (!scanner.hasNextLine()) {
                throw new InvalidDataException(
                        "Too few rows provided");
            }
            String line = scanner.nextLine();
            if (line.length() != cols) {
                throw new InvalidDataException(
                        "Too few columns provided");
            }
            for (int j = 0; j < cols; j++) {
                char ch = line.charAt(j);
                CellType cellType;
                switch (ch) {
                    case 'X': // Wall
                        cellType = CellType.Wall;
                        break;
                    case '.': // Space
                        cellType = CellType.Open;
                        break;
                    case 'S': // Start
                        if (hasStart) {
                            throw new InvalidDataException(
                                "More than one starting point provided");
                        }
                        cellType = CellType.Start;
                        hasStart = true;
                        break;
                    case 'T': // Target
                        if (hasFinish) {
                            throw new InvalidDataException(
                                "More than one end point provided");
                        }
                        cellType = CellType.Target;
                        hasFinish = true;
                        break;
                    default:
                        throw new InvalidDataException(
                                "Provided cell not valid");
                }
                Location loc = new Location(i, j, cellType);
                maze[i][j] = loc;
                if (cellType == CellType.Start) {
                    startLocation = loc;
                }
            }
        }
        
        if (!hasStart) {
            throw new InvalidDataException(
                    "No starting location provided");
        }
        
        if (!hasFinish) {
            throw new InvalidDataException(
                    "No target location provided");
        }
        
        Queue<Location> queue = new LinkedList<>();
        queue.add(startLocation);
        
        Location goal = null;
        
        while (!queue.isEmpty()) {
            Location current = queue.remove();
            if (current.isGoal()) {
                goal = current;
                break;
            }
            for (Location n : getNeighbors(maze, current)) {
                if (n == null) continue; // Skip non-existant neighbors
                if (n.isUnvisited()){
                    n.markVisited();
                    if (n.isOpen()) {
                        n.setPrevious(current);
                        queue.add(n);
                    }
                }
            }
            current.markExamined();
        }
        
        if (goal == null) {
            System.out.println("No solution found");
            return;
        }
        
        System.out.println("Total distance = " + printResults(goal));
    }
    
    /**
     * Prints the results of the search recursively,
     *  reversing the list as it goes
     * @param cell the goal cell
     * @return the number of cells in the solution path
     */
    private static int printResults(Location cell) {
        int i = 0; // Running count of previous sums
        if (cell.getPrevious() != null) {
            // Call print results for the previous node and tally the sum
            i = printResults(cell.getPrevious()) + 1;
        }
        // Output the results
        System.out.println(
               "<" + cell.getRow() + " " + cell.getColumn() + ">");
        return i;
    }
    
    /**
     * Gets the neighbors of the provided cell in the given map
     * @param map the map to use
     * @param cell the cell to search for neighbors on
     * @return the neighbors, if they exist
     */
    private static Location[] getNeighbors(Location[][] map,
            Location cell) {
        int rows = map.length;
        int cols = map[0].length;
        int row = cell.getRow();
        int col = cell.getColumn();
        Location[] neighbors = new Location[4]; // 4 possible neighbors
        
        // Check every possible location for a neighbor
        if (row - 1 >= 0) {
            neighbors[0] = map[row - 1][col];
        }
        if (col + 1 < cols) {
            neighbors[1] = map[row][col + 1];
        }
        if (row + 1 < rows) {
            neighbors[2] = map[row + 1][col];
        }
        if (col - 1 >= 0) {
            neighbors[3] = map[row][col - 1];
        }
        
        return neighbors;
    }
}

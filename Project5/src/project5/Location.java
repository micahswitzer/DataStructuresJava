package project5;

/**
 * Stores the location and other information about a given cell in the map
 * @author Micah Switzer
 */
public class Location {
    private final int row;
    private final int column;
    private final CellType type;
    private Mark mark;
    private Location previous = null;
    
    
    public Location(int x, int y, CellType type) {
        this.row = x;
        this.column = y;
        this.type = type;
        this.mark = Mark.Unvisited;
    }
    
    /**
     * Marks the cell as visited
     */
    public void markVisited() {
        mark = Mark.Visited;
    }
    
    /**
     * Marks the cell as examined
     */
    public void markExamined() {
        mark = Mark.Examined;
    }
    
    /**
     * Gets whether or not this cell can be traversed
     * @return whether or not this cell can be traversed
     */
    public boolean isOpen() {
        return type == CellType.Open || type == CellType.Target;
    }
    
    /**
     * Gets whether or not this cell is the goal
     * @return whether or not this cell is the goal
     */
    public boolean isGoal() {
        return type == CellType.Target;
    }
    
    /**
     * Gets whether of not this cell is unvisited
     * @return whether or not this cell is unvisited
     */
    public boolean isUnvisited() {
        return mark == Mark.Unvisited;
    }
    
    /**
     * Gets the row of this cell
     * @return the row of this cell
     */
    public int getRow() {
        return row;
    }
    
    /**
     * Gets the column of the cell
     * @return the column number of this cell
     */
    public int getColumn() {
        return column;
    }
    
    /**
     * Gets the previous cell
     * @return the previous cell
     */
    public Location getPrevious() {
        return previous;
    }
    
    /**
     * Sets the previous location of this cell
     * @param previous the previous location of this cell
     */
    public void setPrevious(Location previous) {
        this.previous = previous;
    }
    
    /**
     * Stores the marked status of this cell
     */
    private enum Mark {
        Unvisited,
        Visited,
        Examined
    }
}

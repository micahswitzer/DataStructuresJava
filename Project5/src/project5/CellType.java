package project5;

/**
 * Each possible type of cell
 * @author Micah Switzer
 */
public enum CellType {
    Wall,
    Open,
    Start,
    Target
}

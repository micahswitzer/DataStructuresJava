package homework;

public class NodeListStack<E> {
    private PositionalList<E> list = new NodePositionList<E>();
    
    int size() {
        return list.size();
    }
    boolean isEmpty() {
        return list.isEmpty();
    }
    void push(E e) {
        list.addFirst(e);
    }
    E top() throws StackEmptyException {
        if (isEmpty()) {
            throw new StackEmptyException();
        }
        return list.first().getElement();
    }
    E pop() throws StackEmptyException {
        if (isEmpty()) {
            throw new StackEmptyException();
        }
        return  list.remove(list.first());
    }
}

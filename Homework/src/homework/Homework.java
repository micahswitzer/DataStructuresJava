package homework;

/**
 *
 * @author Micah Switzer
 */
public class Homework {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        node head = new node(0, new node(1, new node(2, new node(3, null))));
        node temp = head;
        while (temp != null) {
            System.out.println(temp.data);
            temp = temp.next;
        }
        System.out.println("---REV---");
        temp = reverseNodes(head);
        while (temp != null) {
            System.out.println(temp.data);
            temp = temp.next;
        }
    }
    
    public static int maxElement(int[] data) {
        if (data.length == 1) {
            return data[0];
        }
        int middleIdx = data.length / 2;
        int[] arrLeft = new int[middleIdx];
        int[] arrRight = new int[data.length - middleIdx];
        for (int i = 0; i < middleIdx; i++) {
            arrRight[i] = data[middleIdx + i];
            arrLeft[i] = data[i];
        }
        if (data.length % 2 == 1) {
            arrRight[middleIdx] = data[middleIdx * 2];
        }
        return java.lang.Integer.max(maxElement(arrLeft), maxElement(arrRight));
    }
    
    public static int strToInt(String num) {
        if (num.length() == 1) {
            return num.charAt(0) - '0';
        }        
        return strToInt(num.substring(0, num.length() - 1)) * 10 + strToInt(num.substring(num.length() - 1));
    }
    
    static node reverseNodes(node head) {
        node first = head;
        node next = head.next;
        if (next == null) {
            return null;
        }
        
        node finalNode = reverseNodes(next);
        
        first.next.next = first;
        first.next = null;
        
        if (finalNode!=null) {
            return finalNode;
        }
        return next;
    }
}

class node {
    public node(int d, node n){
        data = d;
        next = n;
    }
    public int data;
    public node next;
}

package homework;

public interface Position<E> {
    E getElement() throws IllegalStateException;
}

package project7.tests;

import org.junit.Test;
import project7.*;

import java.util.Random;

import static org.junit.Assert.*;

public class ArrayHeapTest {

    @Test
    public void addRemove() {
        Random rand = new Random(); // Used for creating random keys
        // Use the maximum size to avoid resizing
        Heap heap = new ArrayHeap(new IntegerComparator(), 10000);
        for (int i = 0; i < 10000; i++) { // Iterate to add 10000 random items to the heap
            heap.add(rand.nextInt(), i);
        }
        Item lastItem = (Item)heap.removeRoot(); // Prime the testing loop by removing the first item
        while (!heap.isEmpty()) { // Keep removing until empty
            Item currentItem = (Item)heap.removeRoot();
            // Check if the key we just removed was greater or equal to the previous node
            assertTrue((int)currentItem.key() >= (int)lastItem.key());
            lastItem = currentItem;
        }
    }

    @Test(expected = InvalidObjectException.class)
    public void invalidKey() {
        Heap heap = new ArrayHeap(new IntegerComparator());
        heap.add("Invalid Key", "Valid Element"); // Try using a string as the key
    }

    @Test(expected = EmptyHeapException.class)
    public void emptyHeap() {
        Heap heap = new ArrayHeap(new IntegerComparator());
        heap.removeRoot(); // Try removing when the heap is empty
    }

}
package project7;

/**
 * Simple implementation of a heap using an ArrayBinaryTree as the underlying structure
 * @author Micah Switzer
 * @version 1.0
 */

public class ArrayHeap extends ArrayBinaryTree implements Heap {

    Comparator heapComp;

    public ArrayHeap(Comparator newComp) {
        this (newComp, DEFAULT_SIZE);
    }

    public ArrayHeap(Comparator newComp, int newSize) {
        super (newSize);
        heapComp = newComp;
    }

    /**
     * Adds a key-value pair to the heap
     * @param newKey the key to use; must be comparable using the provided comparator
     * @param newElement the value in the key-value pair
     * @throws InvalidObjectException when the key can't be compared using the desired comparator
     */
    @Override
    public void add(Object newKey, Object newElement) throws InvalidObjectException {
        if (!heapComp.isComparable(newKey)) { // check if we can compare this key
            throw new InvalidObjectException("The provided key cannot be compared using the provided comparator");
        }
        // create a new position object
        ArrayPosition pos = new ArrayPosition(this.size(), new Item(newKey, newElement));
        // check if we need to resize
        if (this.btArray.length == this.size()) {
            resize();
        }
        // store the position object and increment the size
        this.btArray[this.size++] = pos;
        // bubble that baby up
        bubbleUp(pos);
    }

    /**
     * Removes and returns the root of the heap, this will be the smallest item by default
     * @return the smallest item on the heap
     * @throws EmptyHeapException when the heap is empty
     */
    @Override
    public Object removeRoot() throws EmptyHeapException {
        if (isEmpty()) { // can't remove from an empty heap
            throw new EmptyHeapException("Cannot remove the root of an empty heap");
        }
        ArrayPosition root = (ArrayPosition) this.root();
        if (size() == 1) { // don't worry about switching and bubbling if there's only one element left
            this.btArray[0] = null; // clear the last element
            size = 0;
        }
        else {
            // swap the root with the last node
            swapNodes(root, this.btArray[size() - 1]);
            btArray[root.getIndex()] = null; // null out the old root node
            ArrayPosition newRoot = (ArrayPosition) this.root(); // grab the new root
            size--; // decrease the size
            bubbleDown(newRoot); // bubble it down
        }
        return root.element(); // return the old root
    }

    /**
     * bubbles the item described by the position object provided up the heap into a valid location
     * @param pos the position of the item to bubble up
     */
    private void bubbleUp(ArrayPosition pos) {
        ArrayPosition parent = (ArrayPosition) parent(pos); // the parent position as an ArrayPosition
        if (parent == null) {
            return; // stop bubbling up if we're the root node
        }
        // check if the element is less than it's parent
        if (heapComp.isLessThan(((Item)pos.element()).key(), ((Item)parent.element()).key())) {
            // swap up and bubble up
            swapNodes(parent, pos);
            bubbleUp(pos);
        }
    }

    /**
     * Swaps two nodes in the heap
     * @param pos1 the first node to swap
     * @param pos2 the second node to swap
     */
    private void swapNodes(ArrayPosition pos1, ArrayPosition pos2) {
        int idx = pos1.getIndex(); // get the index of the first node
        pos1.setIndex(pos2.getIndex()); // set the index of the first node to the index of the second node
        pos2.setIndex(idx); // set the index of the second node to the index that was recorded
        this.btArray[pos1.getIndex()] = pos1; // switch the position objects inside of the array
        this.btArray[pos2.getIndex()] = pos2;
    }

    /**
     * bubbles the item described by the position object provided down the heap into a valid location
     * @param pos the position of the item to bubble down
     */
    private void bubbleDown(ArrayPosition pos) {
        int posKey = getKey(pos);
        Position leftChild = leftChild(pos);
        if (leftChild == null) {
            // don't need to bubble down anymore if we're already at the bottom
            return;
        }
        // get the keys of both children
        int leftChildKey = getKey(leftChild);
        Position rightChild = rightChild(pos);
        int rightChildKey = -1;
        if (rightChild != null) {
            // don't want to try getting the key if it doesn't exist
            rightChildKey = getKey(rightChild);
        }
        // store the results of the comparisons for later use
        boolean isGreaterLeft = heapComp.isGreaterThan(posKey, leftChildKey);
        boolean isGreaterRight = heapComp.isGreaterThan(posKey, rightChildKey);
        if (isGreaterLeft) {
            // check the right child as well, in which case we need to figure out which child is the smallest
            if (rightChildKey != -1 && isGreaterRight) {
                if (heapComp.isLessThan(leftChildKey, rightChildKey)) {
                    // left was smaller, swap
                    swapNodes(pos, (ArrayPosition) leftChild);
                }
                else {
                    // right was smaller, swap
                    swapNodes(pos, (ArrayPosition) rightChild);
                }
            }
            else {
                // only the left child is smaller so swap with that one
                swapNodes(pos, (ArrayPosition) leftChild);
            }
            // we swapped so let's bubble down again
            bubbleDown(pos);
        }
        else if (rightChildKey !=-1 && isGreaterRight) {
            // only the right child is smaller and it exists so swap with that one
            swapNodes(pos, (ArrayPosition) rightChild);
            // we swapped, bubble down again
            bubbleDown(pos);
        }
    }

    /**
     * helper function that gets the key from a position object
     * @param pos the position to get the key of
     * @return the key of the provided position
     */
    private int getKey(Position pos) {
        return (int)((Item)pos.element()).key();
    }

    /**
     * resizes the internal data array to twice it's current size and copies over previous elements
     */
    private void resize() {
        // create a new array
        ArrayPosition[] newArray = new ArrayPosition[this.btArray.length * 2];
        // copy over all elements from the old array
        for (int i = 0; i < this.btArray.length; i++) {
            newArray[i] = this.btArray[i];
        }
        // switch out the arrays
        this.btArray = newArray;
    }

    public static void main (String[] args) {
	    Comparator myComp = new IntegerComparator();
        ArrayHeap myHeap = new ArrayHeap (myComp, 8);

        myHeap.add(new Integer(14),new Integer(14));
        myHeap.add(new Integer(17),new Integer(17));
        myHeap.add(new Integer(3),new Integer(3));
        myHeap.add(new Integer(2),new Integer(21));
        myHeap.add(new Integer(8),new Integer(8));
        myHeap.add(new Integer(7),new Integer(18));
        myHeap.add(new Integer(1),new Integer(1));
        myHeap.add(new Integer(19),new Integer(11));
        myHeap.add(new Integer(17),new Integer(17));
        myHeap.add(new Integer(25),new Integer(6));

        System.out.println(myHeap.size());

        while (!myHeap.isEmpty()) {

            Item removedItem = (Item) myHeap.removeRoot();
            System.out.print("Key:   " + removedItem.key() + "     ");
            System.out.println("Removed " + removedItem.element());
        }
        System.out.println("All nodes removed");
    }
}
package project7;

/**
 * Title:        Project #7
 * Description:
 * Copyright:    Copyright (c) 2001
 * Company:
 * @author
 * @version 1.0
 */

public interface Heap extends BinaryTree {
  // may want to throw FullHeapException unless an extensible array used
  void add(Object newKey, Object newElement) throws InvalidObjectException;

  Object removeRoot() throws EmptyHeapException;
}

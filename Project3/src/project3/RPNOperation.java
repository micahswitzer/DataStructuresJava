package project3;

/**
 * Interface used for operation lambdas
 * @author Micah Switzer
 */
public interface RPNOperation {
    /**
     * Lambda compute function
     * @param args the args passed from the parser
     * @return the result of the operation
     */
    double compute(double[] args);
}
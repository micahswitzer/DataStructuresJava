package project3;

/**
 *
 * @author Micah Switzer
 */
public class InvalidRPNString extends Exception {
    public InvalidRPNString() {
        super();
    }
    public InvalidRPNString(String message) {
        super(message);
    }
}

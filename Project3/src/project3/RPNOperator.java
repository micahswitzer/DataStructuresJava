package project3;

/**
 * represents an operator and it's associated operation
 * @author Micah Switzer
 */
public class RPNOperator {
    private final String opString;
    private final int numArgs;
    private final RPNOperation operation;
    
    /**
     * 
     * @param opStr string that represents the operator
     * @param numArgs the number of arguments the operator requires
     * @param operation the action to be performed
     */
    public RPNOperator(String opStr, int numArgs, RPNOperation operation) {
        this.opString = opStr;
        this.numArgs = numArgs;
        this.operation = operation;
    }
    
    public String getOpString() {
        return opString;
    }
    
    public int getNumArgs() {
        return numArgs;
    }
    
    public RPNOperation getOperation() {
        return operation;
    }
}

package project3;

import java.util.Scanner;
import java.util.Stack;
import java.util.Map;
import java.util.HashMap;

/**
 * Class to parse and compute RPN strings
 * @author Micah Switzer
 */
public class RPNParser {
    
    /**
     * Stack that holds all of the numbers
     */
    private final Stack<Double> numStack = new Stack<>();
    /**
     * Hash map to store all of the operators for quick look up
     */
    private final Map<String, RPNOperator> ops = new HashMap<>();
    
    /**
     * Creates a new RPNParser class with default operators
     */
    public RPNParser() {
        this(true);
    }
    
    /**
     * Creates a new RPNParser class with the option
     *  to omit default operators
     * @param addDefaultOps if false,
     *  no operators are added by default
     */
    public RPNParser(boolean addDefaultOps) {
        if (addDefaultOps) {
            initOps();
        }
    }
    
    /**
     * adds the default operators to this instance
     */
    private void initOps() {
        addOperator(new RPNOperator("+", 2, p -> p[0] + p[1]));
        addOperator(new RPNOperator("-", 2, p -> p[1] - p[0]));
        addOperator(new RPNOperator("*", 2, p -> p[0] * p[1]));
        addOperator(new RPNOperator("/", 2, p -> p[1] / p[0]));
    }
    
    /**
     * Adds an operator to the operator Map
     * @param operator the operator to add
     */
    public void addOperator(RPNOperator operator) {
        ops.put(operator.getOpString(), operator);
    }
    
    /**
     * pushes a number onto the number stack
     * @param x 
     */
    private void pushNumber(double x) {
        numStack.push(x);
    }
    
    /**
     * performs a single operation on the stack
     * @param op the string that represent the operation to perform
     * @throws InvalidRPNString 
     */
    private void doOperation(String op) throws InvalidRPNString {
        RPNOperator rpnOp = ops.get(op);
        if (rpnOp == null) {
            throw new InvalidRPNString("Operator '" + op
                    + "' not recognized");
        }
        double[] vals = popValues(rpnOp.getNumArgs());
        double result;
        try {
            result = rpnOp.getOperation().compute(vals);
        }
        catch (java.lang.Exception ex) {
            throw new InvalidRPNString(
                    "Operation '" + op + "' failed to compute");
        }
        if (result == Double.POSITIVE_INFINITY
                || result == Double.NEGATIVE_INFINITY) {
            throw new InvalidRPNString("Cannot devide by zero");
        }
        pushNumber(result);
    }
    
    /**
     * pops a value from the number stack
     * @return the value that was popped
     */
    private double popValue() {
        return numStack.pop();
    }
    
    /**
     * pops multiple values from the number stack
     * @param numVals the number of values to pop
     * @return the values as an array
     * @throws InvalidRPNString if there are too few values on the stack
     */
    private double[] popValues(int numVals) throws InvalidRPNString {
        if (this.numStack.size() < numVals)
        {
            throw new InvalidRPNString("Too few values on the stack");
        }
        double[] res = new double[numVals];
        for (int i = 0; i < numVals; i++) {
            res[i] = popValue();
        }
        return res;
    }
    
    /**
     * clears the number stack
     */
    public void clear() {
        numStack.clear();
    }
    
    /**
     * computes an RPN string
     * @param rpnString the string to compute
     * @return the result of the operations
     * @throws InvalidRPNString 
     */
    public double computeRpn(String rpnString) throws InvalidRPNString {
        Scanner sc = new Scanner(rpnString);
        numStack.clear();
        while (sc.hasNext()) {
            if (sc.hasNextDouble()) {
                pushNumber(sc.nextDouble());
            }
            else {
                doOperation(sc.next());
            }
        }
        if (numStack.size() != 1){
            throw new InvalidRPNString(
                "More than one value is left on the stack"
                        + " after finishing computations");
        }
        // Pop the next value in the stack once we have
        //  nothing left to parse
        return popValue();
    }
}
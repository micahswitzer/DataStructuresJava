package project3.tests;

import project3.*;
import org.junit.Test;
import org.junit.Rule;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;

/**
 * Test class for the RPNParser class
 * @author Micah Switzer
 */
public class RPNParserTest {
    
    @Test
    public void addition() throws InvalidRPNString {
        RPNParser parser = new RPNParser();
        double result = parser.computeRpn("1 1 +");
        assertEquals(2, result, 0.00001);
    }
    
    @Test
    public void subtraction() throws InvalidRPNString {
        RPNParser parser = new RPNParser();
        double result = parser.computeRpn("2 1 -");
        assertEquals(1, result, 0.00001);
    }
    
    @Test
    public void multiplication() throws InvalidRPNString {
        RPNParser parser = new RPNParser();
        double result = parser.computeRpn("2 4 *");
        assertEquals(8, result, 0.00001);
    }
    
    @Test
    public void division() throws InvalidRPNString {
        RPNParser parser = new RPNParser();
        double result = parser.computeRpn("8 2 /");
        assertEquals(4, result, 0.00001);
    }
    
    @Test
    public void complex1() throws InvalidRPNString {
        RPNParser parser = new RPNParser();
        double result = parser.computeRpn("5 5 * 10 / 0.5 + 2 -");
        assertEquals(1, result, 0.00001);
    }
    
    @Test
    public void complex2() throws InvalidRPNString {
        RPNParser parser = new RPNParser();
        double result = parser.computeRpn("1.5 1.5 2.5 + + 0.5 +");
        assertEquals(6, result, 0.00001);
    }
    
    @Test
    public void customOpCos() throws InvalidRPNString {
        RPNParser parser = new RPNParser(false); // Don't need default ops
        parser.addOperator(
                new RPNOperator("cos", 1, p -> java.lang.Math.cos(p[0])));
        double result = parser.computeRpn("0 cos");
        assertEquals(1, result, 0.00001);
    }
    
    @Test
    public void customOpNegate() throws InvalidRPNString {
        RPNParser parser = new RPNParser(false); // Don't need default ops
        parser.addOperator(
                new RPNOperator("--", 1, p -> -p[0]));
        double result = parser.computeRpn("2 --");
        assertEquals(-2, result, 0.00001);
    }
    
    @Test
    public void customOpThreeArgs() throws InvalidRPNString {
        RPNParser parser = new RPNParser(false); // Don't need default ops
        parser.addOperator(
                new RPNOperator("f1", 3, p -> (p[1] + p[2]) / p[0]));
        double result = parser.computeRpn("1 2 3 f1");
        assertEquals(1, result, 0.00001);
    }
    
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    
    @Test
    public void tooFewNumbers() throws InvalidRPNString {
        expectedEx.expect(InvalidRPNString.class);
        expectedEx.expectMessage("Too few values on the stack");
        RPNParser parser = new RPNParser();
        // Don't care what the result is since we don't expect to get one
        parser.computeRpn("1 +");
    }
    
    @Test
    public void operatorNotFound() throws InvalidRPNString {
        expectedEx.expect(InvalidRPNString.class);
        expectedEx.expectMessage("Operator '%' not recognized");
        RPNParser parser = new RPNParser();
        // Don't care what the result is since we don't expect to get one
        parser.computeRpn("10 5 %");
    }
    
    @Test
    public void divideByZero() throws InvalidRPNString {
        expectedEx.expect(InvalidRPNString.class);
        expectedEx.expectMessage("Cannot devide by zero");
        RPNParser parser = new RPNParser();
        // Don't care what the result is since we don't expect to get one
        parser.computeRpn("10 0 /");
    }
    
    @Test
    public void tooManyValues() throws InvalidRPNString {
        expectedEx.expect(InvalidRPNString.class);
        expectedEx.expectMessage("More than one value is left on"
                + " the stack after finishing computations");
        RPNParser parser = new RPNParser();
        // Don't care what the result is since we don't expect to get one
        parser.computeRpn("1 1");
    }
}

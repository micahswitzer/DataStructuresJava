package project4;

/**
 * Simple interface for a queue data structure
 * @author Micah Switzer
 */
public interface Queue<E> {
    /**
     * Enqueues an item on the queue
     * @param element the element to enqueue
     * @throws InvalidDataException if the element is null
     */
    public void enqueue (E element) throws InvalidDataException;
    /**
     * Dequeues an item from the queue
     * @return the frontmost element in the queue
     * @throws QueueEmptyException if the queue is empty
     */
    public E dequeue () throws QueueEmptyException;
    /**
     * Gets the frontmost item of the queue without removing it
     * @return the frontmost element in the queue
     * @throws QueueEmptyException if the queue is empty
     */
    public E front () throws QueueEmptyException;
    /**
     * Gets the number of elements in the queue
     * @return the number of elements in the queue
     */
    public int size();
    /**
     * Gets whether the queue is empty or not
     * @return whether the queue is empty or not
     */
    public boolean isEmpty();
}
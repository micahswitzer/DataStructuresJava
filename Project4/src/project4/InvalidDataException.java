package project4;

/**
 * Thrown when bad data is given to be enqueued
 * @author Micah Switzer
 */
public class InvalidDataException extends Exception {
    public InvalidDataException() {
        super();
    }
    public InvalidDataException(String message){
        super(message);
    }
}

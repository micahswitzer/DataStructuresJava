package project4;

/**
 * Thrown when a dequeue is requested but the queue is empty
 * @author Micah Switzer
 */
public class QueueEmptyException extends Exception {
    public QueueEmptyException() {
        super();
    }
    public QueueEmptyException(String message){
        super(message);
    }
}

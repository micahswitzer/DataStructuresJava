package project4;

/**
 * A queue implementation utilizing an array as its internal data structure
 * @author Micah Switzer
 * @param <E> The type of object to store in the queue
 */
public class ArrayQueue<E> implements Queue<E> {

    private E[] items;
    private int size = 0;
    private int start = 0;
    
    public ArrayQueue() {
        // Default capacity is 10
        this(10);
    }
    
    public ArrayQueue(int initialCapacity) {
        if (initialCapacity <= 0) {
            // Default to 10 on invalid size input
            initialCapacity = 10;
        }
        items = (E[])new Object[initialCapacity];
    }
    
    @Override
    public void enqueue(E element) throws InvalidDataException {
        if (element == null) {
            throw new InvalidDataException("Can't enque a null element");
        }
        if (size + 1 == items.length) {
            // Expand if we need more room
            expand();
        }
        items[(start + size++) % items.length] = element;
    }
    
    /**
     * Expands the internal array to create a larger capacity
     */
    private void expand() {
        // Create a new array that is twice the length of the original
        E[] newItems = (E[])new Object[items.length * 2];
        for (int i = 0; i < size; i++) { // Copy items to the new array
            newItems[i] = items[(start + i) % items.length];
        }
        items = newItems; // Replace the old array
        start = 0; // Reset the staring position
    }

    @Override
    public E dequeue() throws QueueEmptyException {
        E element = front(); // Get the frontmost item
        items[start] = null; // Null it out
        start = (start + 1) % items.length; // Move the starting position
        size--;
        return element; // Return the value
    }

    @Override
    public E front() throws QueueEmptyException {
        if (isEmpty()) {
            // Throw an exception if the array is empty
            throw new QueueEmptyException();
        }
        return items[start];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }
}

package project4.tests;

import project4.*;
import org.junit.Test;
import org.junit.Rule;
import static org.junit.Assert.*;
import org.junit.rules.ExpectedException;

/**
 * Test class thats test the functionality of the ArrayQueue class
 * @author Micah Switzer
 */
public class QueueTests {
    
    @Test
    public void enqueue()
            throws InvalidDataException, QueueEmptyException {
        Queue<Integer> queue = new ArrayQueue<>();
        queue.enqueue(0);
        assertEquals((int)0, (int)queue.front());
    }
    
    @Test
    public void dequeue()
            throws InvalidDataException, QueueEmptyException {
        Queue<Integer> queue = new ArrayQueue<>();
        queue.enqueue(0);
        assertEquals((int)0, (int)queue.dequeue());
    }
    
    @Test
    public void fillNoExpand()
            throws InvalidDataException, QueueEmptyException {
        Queue<Integer> queue = new ArrayQueue<>();
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        assertEquals((int)0, (int)queue.dequeue());
        assertEquals((int)1, (int)queue.dequeue());
        assertEquals((int)2, (int)queue.dequeue());
        assertEquals((int)3, (int)queue.dequeue());
        assertEquals((int)4, (int)queue.dequeue());
        assertEquals((int)5, (int)queue.dequeue());
        assertEquals((int)6, (int)queue.dequeue());
        assertEquals((int)7, (int)queue.dequeue());
        assertEquals((int)8, (int)queue.dequeue());
        assertEquals((int)9, (int)queue.dequeue());
    }
    
    @Test
    public void wrapNoExpand()
            throws InvalidDataException, QueueEmptyException {
        Queue<Integer> queue = new ArrayQueue<>();
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        assertEquals((int)0, (int)queue.dequeue());
        assertEquals((int)1, (int)queue.dequeue());
        assertEquals((int)2, (int)queue.dequeue());
        assertEquals((int)3, (int)queue.dequeue());
        assertEquals((int)4, (int)queue.dequeue());
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        assertEquals((int)5, (int)queue.dequeue());
        assertEquals((int)6, (int)queue.dequeue());
        assertEquals((int)7, (int)queue.dequeue());
        assertEquals((int)8, (int)queue.dequeue());
        assertEquals((int)9, (int)queue.dequeue());
    }
    
    @Test
    public void fillAndExpand()
            throws InvalidDataException, QueueEmptyException {
        Queue<Integer> queue = new ArrayQueue<>();
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        queue.enqueue(10);
        queue.enqueue(11);
        queue.enqueue(12);
        queue.enqueue(13);
        queue.enqueue(14);
        assertEquals((int)0, (int)queue.dequeue());
        assertEquals((int)1, (int)queue.dequeue());
        assertEquals((int)2, (int)queue.dequeue());
        assertEquals((int)3, (int)queue.dequeue());
        assertEquals((int)4, (int)queue.dequeue());
        assertEquals((int)5, (int)queue.dequeue());
        assertEquals((int)6, (int)queue.dequeue());
        assertEquals((int)7, (int)queue.dequeue());
        assertEquals((int)8, (int)queue.dequeue());
        assertEquals((int)9, (int)queue.dequeue());
        assertEquals((int)10, (int)queue.dequeue());
        assertEquals((int)11, (int)queue.dequeue());
        assertEquals((int)12, (int)queue.dequeue());
        assertEquals((int)13, (int)queue.dequeue());
        assertEquals((int)14, (int)queue.dequeue());
    }
    
    @Test
    public void wrapAndExpand()
            throws InvalidDataException, QueueEmptyException {
        Queue<Integer> queue = new ArrayQueue<>();
        queue.enqueue(0);
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        assertEquals((int)0, (int)queue.dequeue());
        assertEquals((int)1, (int)queue.dequeue());
        assertEquals((int)2, (int)queue.dequeue());
        assertEquals((int)3, (int)queue.dequeue());
        assertEquals((int)4, (int)queue.dequeue());
        queue.enqueue(5);
        queue.enqueue(6);
        queue.enqueue(7);
        queue.enqueue(8);
        queue.enqueue(9);
        queue.enqueue(10);
        queue.enqueue(11);
        queue.enqueue(12);
        queue.enqueue(13);
        queue.enqueue(14);
        assertEquals((int)5, (int)queue.dequeue());
        assertEquals((int)6, (int)queue.dequeue());
        assertEquals((int)7, (int)queue.dequeue());
        assertEquals((int)8, (int)queue.dequeue());
        assertEquals((int)9, (int)queue.dequeue());
        assertEquals((int)10, (int)queue.dequeue());
        assertEquals((int)11, (int)queue.dequeue());
        assertEquals((int)12, (int)queue.dequeue());
        assertEquals((int)13, (int)queue.dequeue());
        assertEquals((int)14, (int)queue.dequeue());
    }
    
    @Test
    public void isEmpty()
            throws InvalidDataException, QueueEmptyException {
        Queue<Integer> queue = new ArrayQueue<>();
        assertTrue(queue.isEmpty());
        queue.enqueue(0);
        assertFalse(queue.isEmpty());
        queue.dequeue();
        assertTrue(queue.isEmpty());
    }
    
    @Test
    public void size()
        throws InvalidDataException, QueueEmptyException {
        Queue<Integer> queue = new ArrayQueue<>();
        assertEquals(0, queue.size());
        queue.enqueue(0);
        assertEquals(1, queue.size());
        queue.dequeue();
        assertEquals(0, queue.size());
    }
    
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    
    @Test
    public void nullInput() throws InvalidDataException {
        expectedEx.expect(InvalidDataException.class);
        expectedEx.expectMessage("Can't enque a null element");
        Queue<Integer> queue = new ArrayQueue<>();
        queue.enqueue(null);
    }
    
    @Test
    public void noDataFront()
            throws QueueEmptyException {
        expectedEx.expect(QueueEmptyException.class);
        Queue<Integer> queue = new ArrayQueue<>();
        queue.front();
    }
    
    @Test
    public void noDataDequeue()
            throws QueueEmptyException {
        expectedEx.expect(QueueEmptyException.class);
        Queue<Integer> queue = new ArrayQueue<>();
        queue.dequeue();
    }
}
